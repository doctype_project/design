## Веб-сайт: WALKBIKE ##
## Дата создания: ноябрь 2016 года ##
## Команда разработчиков: Doctype ##
Сайт предназначен для любителей  пеших походов и велосипедных поездок. На сайте можно создать свой уникальный маршрут, выбрав несколько видов транспорта. Также можно выбирать или просто смотреть поездки других пользователей. Для зарегистрированных пользователей есть возможность добавлять на карте собственные места, оставлять комментарии, оценки фотографий и поездок.

**Дизайн сайта:** https://drive.google.com/drive/folders/0B_DO_wPiMi-dNUNrd3N6bzhfU0E